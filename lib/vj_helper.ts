import _ from 'lodash';
import { Infra } from '../data/airtable_importer';
import { Day } from './types.d';
import VehicleJourney from './vehicle_journey';
import Train from './train';
import Line from './line';

const mondays = (off: number) => 4 * 52 + 1 - Math.floor((365 * off) / 100);
const circulations = (off: number) => mondays(off) + 3 * 52;

function generateVJ(line: Line, infra: Infra, train: Train, pax: number) {
  const aller = _([Day.Monday, Day.Friday, Day.Saturday, Day.Sunday])
    .map((day) => [day, new VehicleJourney(line, day, true, infra, train, pax)])
    .fromPairs()
    .value();

  const retour = _([Day.Monday, Day.Friday, Day.Saturday, Day.Sunday])
    .map((day) => [
      day,
      new VehicleJourney(line, day, false, infra, train, pax),
    ])
    .fromPairs()
    .value();

  return { aller, retour };
}

const yearCost = (
  vjByDay: { [day: string]: VehicleJourney },
  category: string,
  off: number
): number =>
  vjByDay[Day.Monday].pricesByCategory()[category] * mondays(off) +
  vjByDay[Day.Friday].pricesByCategory()[category] * 52 +
  vjByDay[Day.Saturday].pricesByCategory()[category] * 52 +
  vjByDay[Day.Sunday].pricesByCategory()[category] * 52;

export { generateVJ, mondays, circulations, yearCost };
