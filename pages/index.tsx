import Link from 'next/link';
import { GetStaticProps } from 'next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import _ from 'lodash';
import React from 'react';
import Lines from '../data/lines';
import { Infra, importAirtable } from '../data/airtable_importer';
import { fmt } from '../lib/helpers';
import VehicleJourney from '../lib/vehicle_journey';
import { Day } from '../lib/types.d';
import { generateVJ, yearCost } from '../lib/vj_helper';
import trains from '../data/trains';

export const getStaticProps: GetStaticProps = async () => ({
  props: {
    infra: await importAirtable(),
  },
});

const Home = ({ infra }: { infra: Infra }) => {
  const [occupancy, setOccupancy] = React.useState(80);
  const [off, setOff] = React.useState(10);
  const [currentTrain, setCurrentTrain] = React.useState(trains.trenHotel);

  const vjs = _.mapValues(
    Lines,
    (line) => new VehicleJourney(line, Day.Monday, true, infra)
  );

  const yearVjs = _.mapValues(Lines, (line) =>
    generateVJ(
      line,
      infra,
      currentTrain,
      currentTrain.capacity() * (occupancy / 100)
    )
  );

  const priceByCat = _.mapValues(yearVjs, ({ aller, retour }) => ({
    energy: yearCost(aller, 'energy', off) + yearCost(retour, 'energy', off),
    tracks: yearCost(aller, 'tracks', off) + yearCost(retour, 'tracks', off),
    station: yearCost(aller, 'station', off) + yearCost(retour, 'station', off),
  }));

  return (
    <div className="p-12">
      <h1>Furlong : estimation de prix de sillons</h1>
      <div>
        <label htmlFor="off">
          Indisponiblité (maintenance + aléas) :&nbsp;
          <input
            type="number"
            id="off"
            value={off}
            min="0"
            max="50"
            size={2}
            onChange={(e) => setOff(e.target.valueAsNumber)}
          />
          %
        </label>
      </div>
      <div>
        <label htmlFor="occupancy">
          Occupation des trains :&nbsp;
          <input
            type="number"
            id="occupancy"
            value={occupancy}
            min="0"
            max="100"
            size={2}
            onChange={(e) => setOccupancy(e.target.valueAsNumber)}
          />
          %
        </label>
      </div>
      <select onChange={(train) => setCurrentTrain(trains[train.target.value])}>
        {Object.keys(trains).map((key) => (
          <option
            key={key}
            value={key}
            selected={trains[key].label === currentTrain.label}
          >
            {key}
          </option>
        ))}
      </select>
      <div className="px-6 py-2">
        <table className="table-auto">
          <thead>
            <th>Route</th>
            <th>Grande vitesse</th>
            <th>Cout par circulation</th>
            <th>Élec/an</th>
            <th>Voies/an</th>
            <th>Gares/an</th>
            <th>km</th>
            <th>Début</th>
            <th>Fin</th>
            <th>Courants</th>
            <th>Signalisations</th>
            <th>Écartements</th>
          </thead>
          <tbody>
            {_.map(vjs, (vj, id) => (
              <tr key={id}>
                <td>
                  <Link href={`/lines/${id}`}>
                    <a>{vj.label}</a>
                  </Link>
                </td>
                <td className="text-center">
                  <FontAwesomeIcon icon={vj.highspeed() ? 'check' : 'times'} />
                </td>
                <td className="text-right">{fmt(vj.price)}</td>
                <td className="text-right">{fmt(priceByCat[id].energy)}</td>
                <td className="text-right">{fmt(priceByCat[id].tracks)}</td>
                <td className="text-right">{fmt(priceByCat[id].station)}</td>
                <td className="text-right">{fmt(vj.distance)}</td>
                <td>{_.head(vj.edges).edge.departure.label}</td>
                <td>{_.last(vj.edges).edge.arrival.label}</td>
                <td>
                  {_(vj.edges).map('edge.line.current').uniq().join(', ')}
                </td>
                <td>
                  {_(vj.edges).map('edge.line.signaling').uniq().join(', ')}
                </td>
                <td>{_(vj.edges).map('edge.line.gauge').uniq().join(', ')}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Home;
